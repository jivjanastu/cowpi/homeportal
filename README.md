# Home Portal for COWPi (Aamne Saamne Pi)
This is a webpage that loads when the default browser is opened in Pi

## Install instructions
* git clone this repo
* ensure that syskagaz/syskagaz gets executed at startup (TODO: should update it to executing at page load)
* in command line add this option to your browser command
``` 
<browser_command> --app <location_of_repo>/index.html 
```
* you can add options to your existing web server too

## Localization
* To customise field names for system config edit values in `static/localization.json`
