#!/bin/bash

printf "Installing and configuring *** home portal *** \n\n"

############################################

git clone https://gitlab.com/jivjanastu/cowpi/homeportal.git
mv homeportal $HOME/.homeportal
chmod +x $HOME/.homeportal/syskagaz/gensyskagaz.sh

cat > dhcpcd.enter-hook << END
$HOME/.homeportal/syskagaz/gensyskagaz.sh
END

sudo mv dhcpcd.enter-hook /etc/dhcpcd.enter-hook
sudo cp /etc/dhcpcd.enter-hook /etc/dhcpcd.exit-hook

sudo sed -i "s/\%u.*/\%u \-\-app http\:\/\/localhost\:3354/g" /usr/share/raspi-ui-overrides/applications/lxde-x-www-browser.desktop

############################################

cat >> $HOME/.everyboot/everyboot.sh << END

bash $HOME/.homeportal/syskagaz/gensyskagaz.sh

END

sudo sh -c 'cat >> /etc/nginx/sites-available/default << END
server {
	listen 3354;
	listen [::]:3354;

	server_name homeportal;

	root /home/pi/.homeportal;

	index index.html index.htm index.php;

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files \$uri \$uri/ =404;
	}

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
	
		# With php-fpm (or other unix sockets):
		fastcgi_pass unix:/run/php/php7.3-fpm.sock;
		# With php-cgi (or other tcp sockets):
		# fastcgi_pass 127.0.0.1:9000;
	}
}

END'
sudo /etc/init.d/nginx reload


