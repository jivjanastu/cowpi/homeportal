// function to fetch data using fetch api
async function fetchData(url) {
    var jsonData = await fetch(url);
    var response = await jsonData.json();
    return response;
}

// render function
function renderData(data, localized) {
    // get all keys from the json data as array
    var keys = Object.keys(data);
    // create var reference to HTML element
    var container = document.querySelector('#layout-container');
    // create var reference to template
    var rowTemplate = document.querySelector('#row-template');

    // iterate over the keys to render config data
    keys.forEach(key => {
        // for template usage refer: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/template
        var clone = rowTemplate.content.cloneNode(true);
        var leftColumn = clone.querySelector('.data-key');
        var rightColumn = clone.querySelector('.data-value');

        if(data[key]){
          leftColumn.textContent = localized[key];
          rightColumn.textContent = data[key];  
        }
        container.appendChild(clone);

    });

    // function to render events
    function renderEvents(data) {
        // iterate over events data to render
        data.forEach(event => {
            var clone = rowTemplate.content.cloneNode(true);
            var leftColumn = clone.querySelector('.data-key');
            leftColumn.textContent = event.name;
            var rightColumn = clone.querySelector('.data-value');
            // Create a anchor tag for clickable links
            var linkTag = document.createElement('a');
            var linkText = document.createTextNode(event.linkText);
            linkTag.appendChild(linkText);
            linkTag.href = event.link;
            linkTag.target= '_blank'
            //append the link to right column
            rightColumn.appendChild(linkTag)
            container.appendChild(clone);
        });

        
    }

    // fetch events data and call render events function
    fetchData('./static/events.json')
        .then(data => {
            renderEvents(data);
        })
        .catch(reason => console.log(reason.message));
}

function init(){

   // fetch localization data
   fetchData('./static/localization.json')
       .then(localized => {
           //fetch config data and call render function
           fetchData('./syskagaz/syskagaz.json')
               .then(data => {
                   renderData(data, localized);
               })
               .catch(reason => console.log(reason.message));
       })
       .catch(reason => console.log(reason.message));

 
}

// Initialize
init();