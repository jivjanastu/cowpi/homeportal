#!/bin/bash

dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )

cat > $dir/syskagaz.json << END
{
}
END

syskagazpy=$dir/syskagaz.py

python3 $syskagazpy hostname $(hostname)

interfaces=$(route |awk '/default/{print $NF}')
for interface in $interfaces 
do
    python3 $syskagazpy $interface $(ifconfig $interface | awk '/inet /{print $2}')
done

python3 $syskagazpy syncthing $(sudo -u $USER syncthing -device-id)
