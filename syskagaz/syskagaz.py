import os
import sys
import json

projectpath = os.path.split(os.path.realpath(__file__))[0]

with open(projectpath+"/syskagaz.json","r+") as f:
    data = json.load(f)
    data[sys.argv[1]]=sys.argv[2]
    
    f.seek(0)
    json.dump(data,f)
    f.truncate()
